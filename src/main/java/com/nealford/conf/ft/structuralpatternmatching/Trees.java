package com.nealford.conf.ft.structuralpatternmatching;

import fj.data.Either;

import static java.lang.Math.max;

public class Trees {

// BEGIN java_eithertree_depth
static public int depth(Tree t) {
    for (Tree.Empty e : t.toEither().left())
        return 0;
    for (Either<Tree.Leaf, Tree.Node> ln: t.toEither().right()) {
        for (Tree.Leaf leaf : ln.left())
            return 1;
        for (Tree.Node node : ln.right())
            return 1 + max(depth(node.left), depth(node.right));
    }
    throw new RuntimeException("Inexhaustible pattern match on tree");
}
// END java_eithertree_depth

// BEGIN java_eithertree_intree
static public boolean inTree(Tree t, int value) {
    for (Tree.Empty e : t.toEither().left())
        return false;
    for (Either<Tree.Leaf, Tree.Node> ln: t.toEither().right()) {
        for (Tree.Leaf leaf : ln.left())
            return value == leaf.n;
        for (Tree.Node node : ln.right())
            return inTree(node.left, value) | inTree(node.right, value);
    }
    return false;
}
// END java_eithertree_intree

// BEGIN java_eithertree_occurrences
static public int occurrencesIn(Tree t, int value) {
    for (Tree.Empty e: t.toEither().left())
        return 0;
    for (Either<Tree.Leaf, Tree.Node> ln: t.toEither().right()) {
        for (Tree.Leaf leaf : ln.left())
            if (value == leaf.n) return 1;
        for (Tree.Node node : ln.right())
            return occurrencesIn(node.left, value) 
                    + occurrencesIn(node.right, value);
    }
    return 0;
}
// END java_eithertree_occurrences

    static public String printTree(Tree t, String output) {
        for (Tree.Empty e: t.toEither().left())
            return output;
        for (Either<Tree.Leaf, Tree.Node> ln: t.toEither().right()) {
            for (Tree.Leaf leaf : ln.left())
                return output + String.valueOf(leaf.n) + "\n";
            for (Tree.Node node : ln.right())
                return "." + printTree(node.left, output) + printTree(node.right, output);
        }
        return "";
    }

    public static void main(String[] args) {
        Tree t = new Tree.Node(new Tree.Node(new Tree.Node(new Tree.Leaf(4),new Tree.Node(new Tree.Leaf(1), new Tree.Node(new Tree.Node(new Tree.Node(new Tree.Node(
                new Tree.Node(new Tree.Node(new Tree.Leaf(10), new Tree.Leaf(0)), new Tree.Leaf(22)), new Tree.Node(new Tree.Node(new Tree.Node(new Tree.Leaf(4),
                new Tree.Empty()), new Tree.Leaf(101)), new Tree.Leaf(555))), new Tree.Leaf(201)), new Tree.Leaf(1000)), new Tree.Leaf(4)))),
                new Tree.Leaf(12)), new Tree.Leaf(27));
        System.out.println(printTree(t, ""));
    }


}
