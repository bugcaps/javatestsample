package com.nealford.conf.ft.patterns_adaptor;

public interface Circularity {
    public double getRadius();
}
