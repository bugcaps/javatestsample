package javathread;

import java.util.concurrent.CompletableFuture;

import org.junit.jupiter.api.Test;
public class completablefuture {
	Runnable task=()-> {
		try {
			Thread.sleep(5*1000L);
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("TASK completed");
	};
	
	@Test
	public void completableFuture() throws Exception {
		
		CompletableFuture.runAsync(task)
			.thenCompose(aVoid ->CompletableFuture.runAsync(task))
			.thenAcceptAsync(aVoid->System.out.println("all tasks completed!!"))
			.exceptionally (throwable-> {
				System.out.println("Exception");
				return null;
			});
		Thread.sleep(11*1000L);
	}
	
}

